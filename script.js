
const scrollElements = document.querySelectorAll(".js-scroll");

const elementInView = (el, dividend = 1) => {
    const elementTop = el.getBoundingClientRect().top;

    return (
        elementTop <=
        (window.innerHeight || document.documentElement.clientHeight) / dividend
    );
};

const elementOutofView = (el) => {
    const elementTop = el.getBoundingClientRect().top;

    return (
        elementTop > (window.innerHeight || document.documentElement.clientHeight)
    );
};

const displayScrollElement = (element) => {
    element.classList.add("scrolled");
};

const hideScrollElement = (element) => {
    element.classList.remove("scrolled");
};

const handleScrollAnimation = () => {
    scrollElements.forEach((el) => {
        if (elementInView(el, 1.2)) {
            displayScrollElement(el);
        } else if (elementOutofView(el)) {
            hideScrollElement(el);
        }
    })
}

window.addEventListener("scroll", () => {
    handleScrollAnimation();
});


/* link */



/*
// Cache selectors
var lastId;
topMenu = $("#top-menu"),
topMenuHeight = topMenu.outerHeight()+15,
// All list items
menuItems = topMenu.find("a"),
// Anchors corresponding to menu items
scrollItems = menuItems.map(function(){
  var item = $($(this).attr("href"));
  if (item.length) { return item; }
});

// Bind click handler to menu items
// so we can get a fancy scroll animation

// Bind to scroll
$(window).scroll(function(){
// Get container scroll position
var fromTop = $(this).scrollTop()+topMenuHeight;

// Get id of current scroll item
var cur = scrollItems.map(function(){
 if ($(this).offset().top < fromTop)
   return this;
});
// Get the id of the current element
cur = cur[cur.length-1];
var id = cur && cur.length ? cur[0].id : "";

//console.log(fromTop,id, cur,lastId);
if (lastId !== id) {
   lastId = id;
   // Set/remove active class
   // menuItems
     //.parent().removeClass("active")
     //.end().filter("[href='#"+id+"']").parent().addClass("active");
     //menuItems.map(item=> console.log(menuItems[item]).href.includes('#'+id))
     delete menuItems.length;
     delete menuItems.prevObject;
     //menuItems.filter(item=>console.log(menuItems[item].href.includes('#'+id)))

    if(id!='') menuItems.removeClass("active");

     Object.keys(menuItems).map(item=> {
         if(menuItems[item].href.includes('#'+id) && id!=''){
            menuItems[item].classList.add("active")
         }
     });
}
});*/